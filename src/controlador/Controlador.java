package controlador;

import vista.*;
import modelo.*;

import javax.swing.JOptionPane;
import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public class Controlador implements ActionListener{
    private Usuarios user;
    private dbUsuario db;
    private vistaIngreso vistaI;
    private vistaRegistro vistaR;
    private vistaLista vistaL;
    
    public Controlador(vistaIngreso vistaI, vistaRegistro vistaR, vistaLista vistaL, Usuarios user, dbUsuario db){
        this.vistaI = vistaI;
        this.vistaR = vistaR;
        this.vistaL = vistaL;
        this.user = user;
        this.db = db;
        // Escuchar eventos
        this.vistaI.btnIngresar.addActionListener(this);
        this.vistaI.btnRegistrar.addActionListener(this);
        this.vistaR.btnLimpiar.addActionListener(this);
        this.vistaR.btnGuardar.addActionListener(this);
        this.vistaR.btnCerrar.addActionListener(this);
        this.vistaL.btnCerrar.addActionListener(this);
    }
    
    private void inicializarVistaI(String title){
        this.vistaI.setTitle(title);
        this.vistaI.setLocationRelativeTo(null);
        this.vistaI.setVisible(true);
    }
    
    private void inicializarVistaR(String title){
        this.vistaR.setTitle(title);
        this.vistaR.setLocationRelativeTo(null);
        this.vistaR.setVisible(true);
    }
    
    private void inicializarVistaL(String title){
        this.vistaL.setTitle(title);
        this.vistaL.setLocationRelativeTo(null);
        this.cargarDatos();
        this.vistaL.setVisible(true);
    }
    
    private void cargarDatos(){
        ArrayList<Usuarios> lista = new ArrayList<>();
        String columnas[] = {"Id", "Usuario", "Correo"};
        Object[][] filas;
        int numRegistros;
        try{
            numRegistros = this.db.numRegistros();
            filas = new Object[numRegistros][columnas.length];
            lista = this.db.listar();
            for(int i = 0; i < numRegistros; i++){
                filas[i][0] = lista.get(i).getIdUsuarios();
                filas[i][1] = lista.get(i).getNombre();
                filas[i][2] = lista.get(i).getCorreo();
            }
            DefaultTableModel modelo = new DefaultTableModel(filas, columnas);
            this.vistaL.jtbUsuarios.setModel(modelo);
        }
        catch(Exception ex){
            System.err.print("Surgio una excepcion al cargar datos: " + ex.getMessage());
        }
    }
    
    private void limpiar(){
        this.vistaR.txtUsuario.setText("");
        this.vistaR.txtCorreoElectronico.setText("");
        this.vistaR.txtContrasena.setText("");
        this.vistaR.txtReescribirContrasena.setText("");
        this.vistaR.txtUsuario.requestFocusInWindow();
    }
    
    private Boolean isVacio(int v){
        Boolean valor = false;
        switch(v){
            case 1:
                valor = this.vistaI.txtUsuario.getText().equals("")||
                        this.vistaI.txtContrasena.getText().equals("");
                break;
            case 2:
                valor = this.vistaR.txtUsuario.getText().equals("")||
                        this.vistaR.txtCorreoElectronico.getText().equals("")||
                        this.vistaR.txtContrasena.getText().equals("")||
                        this.vistaR.txtReescribirContrasena.getText().equals("");
                break;
        }
        return valor;
    }
    
    private Boolean validarContrasena(){
        return this.vistaR.txtReescribirContrasena.getText().equals(this.vistaR.txtContrasena.getText());
    }
    
    public static void main(String[] args){
        Usuarios user = new Usuarios();
        dbUsuario db = new dbUsuario();
        vistaIngreso vistaI = new vistaIngreso(new JFrame(), true);
        vistaRegistro vistaR = new vistaRegistro(new JFrame(), true);
        vistaLista vistaL = new vistaLista(new JFrame(), true);
        Controlador controlador = new Controlador(vistaI, vistaR, vistaL, user, db);
        controlador.inicializarVistaI("Ingreso");
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.vistaI.btnIngresar){
            if(this.isVacio(1)){
                JOptionPane.showMessageDialog(this.vistaI, "Hay campos vacíos", "Ingreso", JOptionPane.WARNING_MESSAGE);
                return;
            }
            this.user.setNombre(this.vistaI.txtUsuario.getText());
            this.user.setContraseña(this.vistaI.txtContrasena.getText());
            try{
                Usuarios obj = (Usuarios)this.db.buscar(this.user.getNombre());
                if(obj != null){
                    if(!this.vistaI.txtContrasena.getText().equals(obj.getContraseña())){
                        JOptionPane.showMessageDialog(this.vistaI, "La contraseña es incorrecta.", "Ingreso", JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    this.inicializarVistaL("Lista");
                }
                else{
                    JOptionPane.showMessageDialog(this.vistaI, "No se encontró un registro con el nombre " + this.vistaI.txtUsuario.getText() + ". Registrate para acceder.", "Ingreso", JOptionPane.WARNING_MESSAGE);
                }
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vistaI, "Ocurrió un error: " + ex.getMessage(), "Ingreso", JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == this.vistaL.btnCerrar){
            this.vistaL.setVisible(false);
        }
        if(e.getSource() == this.vistaI.btnRegistrar){
            this.inicializarVistaR("Registro");
        }
        if(e.getSource() == this.vistaR.btnGuardar){
            if(this.isVacio(2)){
                JOptionPane.showMessageDialog(this.vistaI, "Hay campos vacíos", "Registro", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(!this.validarContrasena()){
                JOptionPane.showMessageDialog(this.vistaI, "La contraseña es diferente. Asegurate de ingresar la misma contraseña.", "Registro", JOptionPane.WARNING_MESSAGE);
                return;
            }
            try{
                if(this.db.buscar(this.vistaR.txtUsuario.getText()) != null){
                    JOptionPane.showMessageDialog(this.vistaI, "Ya existe este usuario. Prueba con otro nombre.", "Registro", JOptionPane.WARNING_MESSAGE);
                    return;
                }
                this.user.setNombre(this.vistaR.txtUsuario.getText());
                this.user.setCorreo(this.vistaR.txtCorreoElectronico.getText());
                this.user.setContraseña(this.vistaR.txtContrasena.getText());
                this.db.insertar(this.user);
                JOptionPane.showMessageDialog(this.vistaR, "Se registró el usuario " + this.vistaR.txtUsuario.getText() + " con éxito", "Registro", JOptionPane.INFORMATION_MESSAGE);
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(this.vistaI, "Ocurrió un error: " + ex.getMessage(), "Ingreso", JOptionPane.ERROR_MESSAGE);
            }
        }
        if(e.getSource() == this.vistaR.btnLimpiar){
            this.limpiar();
        }
        if(e.getSource() == this.vistaR.btnCerrar){
            this.vistaR.setVisible(false);
        }
    }
}
