package modelo;
import java.util.ArrayList;
public interface dbPersistencia {
    public ArrayList listar() throws Exception;
    public void insertar(Object obj) throws Exception;
    public Object buscar(String codigo) throws Exception;
    public int numRegistros() throws Exception;
}
